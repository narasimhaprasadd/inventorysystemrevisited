package com.Inventory;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class InventoryTest {
    Inventory inventory;
    IpodInventory brazil;
    IpodInventory argentina;

    @Before
    public void setup() {
        brazil = new IpodInventory("Brazil", 100, 100);
        argentina = new IpodInventory("Argentina", 100, 50);
        inventory = new Inventory(brazil, argentina);
    }


    @Test
    public void shouldBeAbleToNotifyWhenThereIsNoSufficientInventory() {
        assertEquals("Out Of Stock", inventory.computeCheapestCost("Argentina", 220));
        assertTrue(brazil.isAvailableInventory(100));
        assertTrue(argentina.isAvailableInventory(100));
    }

    @Test
    public void shouldBeAbleToGetInventoryFromBrazil() {
        assertEquals("500", inventory.computeCheapestCost("Brazil", 5));

        assertTrue(brazil.isAvailableInventory(95));
        assertTrue(argentina.isAvailableInventory(100));
    }

    @Test
    public void shouldBeAbleToGetInventoryFromMultipleInventories() {
        assertEquals("7800", inventory.computeCheapestCost("Argentina", 120));

        assertTrue(argentina.isAvailableInventory(0));
        assertTrue(brazil.isAvailableInventory(80));


    }

    @Test
    public void shouldBeAbleToGetInventoryFromArgentina() {
        assertEquals("4500", inventory.computeCheapestCost("Brazil", 50));

        assertTrue(brazil.isAvailableInventory(100));
        assertTrue(argentina.isAvailableInventory(50));
    }



}
