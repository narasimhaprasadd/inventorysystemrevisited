package com.Inventory;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class IpodInventoryTest {
    @Test
    public void shouldBeAbleToTakeCountryNameInventoryPriceAndReturnThePrice() {
        IpodInventory brazil = new IpodInventory("Brazil", 100, 100);
        assertEquals(1000, brazil.calculatePrice("Brazil", 10));
    }

    @Test
    public void shouldBeAbleToAddShippingAlongWithTheItemPriceForDifferentCountry() {
        IpodInventory brazil = new IpodInventory("Argentina", 100, 100);
        assertEquals(1400, brazil.calculatePrice("Brazil", 10));
    }

    @Test
    public void shouldReturn_0_WhenTheItemQuantityIs_0() {
        IpodInventory brazil = new IpodInventory("Argentina", 100, 100);
        assertEquals(0, brazil.calculatePrice("Brazil", 0));
    }

    @Test
    public void shouldReturn_0_WhenTheItemQuantityIs_Minus_1() {
        IpodInventory brazil = new IpodInventory("Argentina", 100, 100);
        assertEquals(0, brazil.calculatePrice("Brazil", -1));
    }

    @Test
    public void shouldBeAbleToUpdateTheInventory() {
        IpodInventory brazil = new IpodInventory("Argentina", 100, 100);
        brazil.updateInventory(10);
        assertTrue(brazil.isAvailableInventory(90));
    }

    @Test
    public void shoouldKeepInventoryUnchangesWhenNegativeInvntoryIsDrawn() {
        IpodInventory brazil = new IpodInventory("Argentina", 100, 100);
        brazil.updateInventory(-10);
        assertTrue(brazil.isAvailableInventory(100));
    }


    @Test
    public void shouldBeAbleToCheckTheAvailableInventoryIsEqualToReceivedRequest() {
        IpodInventory brazil = new IpodInventory("Argentina", 100, 100);
        assertTrue(brazil.isAvailableInventory(100));
    }

}
