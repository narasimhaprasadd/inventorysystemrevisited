package com.Inventory;

public class Inventory {
    private IpodInventory brazil;
    private IpodInventory argentina;
    private int totalInventory = 0;

    public Inventory(IpodInventory brazil, IpodInventory argentina) {
        this.brazil = brazil;
        this.argentina = argentina;
        getAllInventory();
    }

    private void getAllInventory() {
        int brazilInventory = brazil.getInventory();
        int argentinaInventory = argentina.getInventory();
        totalInventory = brazilInventory + argentinaInventory;
    }


//    //Returns cheapest among two. Only If Complete Inventory is available in a country.
//    public int getCheapest(String country, int quantity) {
//        int costOfBrazil;
//        int costOfArgentina;
//        costOfBrazil = brazil.calculatePrice(country, quantity);
//        costOfArgentina = argentina.calculatePrice(country, quantity);
//        if (costOfBrazil == costOfArgentina) return Integer.MAX_VALUE;
//        if (costOfBrazil < costOfArgentina) {
//            brazil.updateInventory(quantity);
//            return costOfBrazil;
//        }
//        argentina.updateInventory(quantity);
//        return costOfArgentina;
//
//    }
//
//    //Get the cheapest of all Countries,Call from outer world
//    public String computeCheapestCost(String country, int quantity) {
//        if (quantity > totalInventory) return "Out Of Stock";
//        int cheapest = getCheapest(country, quantity);
//        if (cheapest == Integer.MAX_VALUE) return String.valueOf(getCheapPriceFromMultipleInventories(country, quantity));
//        return String.valueOf(cheapest);
//
//    }
//
//    //Take partial values from all countries and return the cheapest cost.
//    private int getCheapPriceFromMultipleInventories(String countryName, int quantity) {
//        int allBrazilItem;
//        int allArgentinaItem;
//
//        allBrazilItem = getItemInOrder(brazil, argentina, countryName, quantity);
//        allArgentinaItem = getItemInOrder(argentina, brazil, countryName, quantity);
//
//        if (allBrazilItem < allArgentinaItem) {
//            updateInventory(brazil, argentina, quantity);
//            return allBrazilItem;
//        }
//        updateInventory(argentina,brazil, quantity);
//        return allArgentinaItem;
//
//    }
//
//    private void updateInventory(IpodInventory firstIpodInventory, IpodInventory secondIpodInventory, int quantity) {
//        firstIpodInventory.updateInventory();
//        secondIpodInventory.updateInventory(firstIpodInventory.getExecessQuantity(quantity));
//    }
//
//    private int getItemInOrder(IpodInventory firstIpodInventory, IpodInventory secondIpodInventory, String countryName, int quantity) {
//        int remainingItemArgentinaFirst = firstIpodInventory.getExecessQuantity(quantity);
//        int partialPrice = firstIpodInventory.calculatePrice(countryName, quantity - remainingItemArgentinaFirst);
//        return partialPrice + secondIpodInventory.calculatePrice(countryName, remainingItemArgentinaFirst);
//
//    }


    public String computeCheapestCost(String country, int quantity) {
        if (totalInventory > quantity) {
            int first = computePrice(argentina, brazil, country, quantity);
            int second = computePrice(brazil, argentina, country, quantity);
            if (first > second) {
                updateInventory(brazil, argentina, quantity);
                return String.valueOf(second);
            }
            updateInventory(argentina, brazil, quantity);
            return String.valueOf(first);
        }
        return "Out Of Stock";
    }

    private void updateInventory(IpodInventory firstInventory, IpodInventory secondInventory, int quantity) {
        int remainingQuantity = quantity - firstInventory.getInventory();
        if (remainingQuantity < 0)
            firstInventory.updateInventory(quantity);
        else {
            firstInventory.updateInventory();
            secondInventory.updateInventory(remainingQuantity);
        }

    }


    private int computePrice(IpodInventory firstInventory, IpodInventory secondInventory, String countryName, int quantity) {
        int remainingQuantity = quantity - firstInventory.getInventory();
        if (remainingQuantity < 0) {
            return firstInventory.calculatePrice(countryName, quantity);
        }
        int availableInventory = firstInventory.getInventory();
        int totalPrice = firstInventory.calculatePrice(countryName, availableInventory);
        totalPrice += secondInventory.calculatePrice(countryName, remainingQuantity);
        return totalPrice;
    }
}
