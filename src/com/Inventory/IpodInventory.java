package com.Inventory;

public class IpodInventory {
    private String name;
    private int quantity;
    private int pricePerUnit;

    public IpodInventory(String name, int inventory, int pricePerUnit) {
        this.name = name;
        this.quantity = inventory;

        this.pricePerUnit = pricePerUnit;
    }
    public int calculatePrice(String countryName, int itemNeeded) {
        if(itemNeeded <= 0 ) return 0;
        if (itemNeeded > quantity) return Integer.MAX_VALUE;

        if (countryName.equals(this.name)) return itemNeeded * pricePerUnit;

        double shippingCharges = Math.ceil(itemNeeded / 10.0) * 400;
        return (int) ((itemNeeded * pricePerUnit) + shippingCharges);
    }

    public void updateInventory(int quantity) {
        if(quantity<0) return;
        this.quantity-=quantity;
    }


    public boolean isAvailableInventory(int inventory) {
        return this.quantity==inventory;
    }

    public void updateInventory() {
        quantity=0;
    }


    public int getInventory() {
        return quantity;
    }


}
